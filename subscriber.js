'use strict';

var SimpleQueue = require("./amqp-simple");

var q = new SimpleQueue("localhost", 5672, null, "domain.product.#", qCallback);

setTimeout(() => {
    for(var i = 0; i < 100; i++) {
        q.pub("domain.product.new", {id: i, name: 'product #' + i})
    }
}, 1500);

function qCallback(message) {
    console.log(message.fields.routingKey, message.content.toString());
}

