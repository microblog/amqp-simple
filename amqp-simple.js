'use strict';

/** 
// Constructor
function Foo(bar) {
    // always initialize all instance properties
    this.bar = bar;
    this.baz = 'baz'; // default value
  }
  // class methods
  Foo.prototype.fooBar = function() {
  
  };
  // export the class
  module.exports = Foo;
*/

// Access the callback-based API

var logger = require('./logger');
var amqp = require('amqplib/callback_api');
const exchange = 'amq.topic';

function start(instance) {
    amqp.connect('amqp://' + instance.host + ':' + instance.port, function(err, conn) {
        if (err) {
            logger.warn("[AMQP]", err.message);
            return setTimeout(() => start(instance), 5000);
        }
        conn.on("error", function(err) {
            if (err.message !== "Connection closing") {
                logger.warn("[AMQP] Connection error", err.message);
            }
        });
        conn.on("close", function() {
            logger.info("[AMQP] reconnecting");
            return setTimeout(() => start(instance), 5000);
        });
        logger.info("[AMQP] connected");
        conn.createChannel(function(err, ch) {
            if(err) {
                logger.error("[AMQP]", err.message);
            }
            ch.assertExchange(exchange, 'topic');
            if(instance.topic) {
                var durable = instance.queue != null;
                var autodelete = instance.queue == null;
                ch.assertQueue(instance.queue, {durable: durable, autoDelete: autodelete}, function(err, q) {
                   if(err) {
                      logger.error("[QKQK]", err.message);
                  }
                ch.bindQueue(q.queue, exchange, instance.topic);
                ch.consume(q.queue, instance.callback, {noAck: true});
                });
            }
            instance.channel = ch;
        });
    });
}


function SimpleQueue(host = 'localhost', port = 5672, queue, topic, callback) {
    this.host = host;
    this.port = port;
    this.channel = null;
    this.queue = queue;
    this.topic = topic;
    this.callback = callback;
    start(this);
}

SimpleQueue.prototype.sub = function(queue, topic, callback) {
    var channel = this.channel;
    channel.assertQueue(queue, {durable: queue != null, autoDelete: queue == null}, function(err, q) {
        channel.bindQueue(q.queue, exchange, topic);
        channel.consume(q.queue, callback, {noAck: true});
    });
}

SimpleQueue.prototype.pub = function(topic, msg) {
    this.channel.publish(exchange, topic, new Buffer(JSON.stringify(msg)));
}

module.exports = SimpleQueue;
