'use strict';

var SimpleQueue = require("./simplequeue");
var q = new SimpleQueue("localhost", 5672, null, null, null);

setTimeout(() => {
    for(var i = 0; i < 100; i++) {
        var id = Math.floor(Math.random() * i);
        q.pub("domain.product.new", {id: id, name: 'product #' + id})
        console.log("Sent...", i );
    }
    //process.exit();
}, 1500);